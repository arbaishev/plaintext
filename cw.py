from pyparsing import *
from collections import OrderedDict
import json
import sys
import argparse


class PlainText():
    def __init__(self):
        self.s = ''
        self.json_result = OrderedDict()
    
    def open_file(self, file):
        f = open(file, 'r')
        for line in f:
            self.s += line

    def makeLit(self, s, val):
        ret = CaselessLiteral(s).setName(s)
        return ret.setParseAction(replaceWith(val))
    
    def parse_text(self):
        typesQuestions = [
            ("[", "text"),
            ("[[", "textarea"),
            ("[[[", "paragraph"),
            ("*", "radio"),
            ("**", "checkbox"),
            ("-", "drop"),
            ("+", "range"),
            ("_", "priority")
            ]
        r = r'\b[a-zA-Z \t0-9#.,!?@$&([\])"%+-]+\b'
        types = Or( [ self.makeLit(s,v) for s,v in typesQuestions ] )

        formObject = Forward()
        formOptions = Forward()

        formTextPass = OneOrMore(Literal('_'))

        formText = OneOrMore(Regex(r))
        formTitle = Suppress('# **') + formText + Suppress('**')
        formDescription = Suppress('# *') + formText + Suppress('*')

        formQuestionTitle = Suppress('#') + formText

        formOptionShortText = types + (formTextPass ^ formText)+ Suppress(']')
        formOptionLongText = types + (formTextPass ^ formText) + Suppress(']]')
        formOptionParagraph = types + formText + Suppress(']]]')

        formOptionRadio = types + formText
        formOptionRadiobuttons = Group(delimitedList(formOptionRadio, delim = LineEnd()))

        formOptionCheckbox = types + formText
        formOptionCheckboxes = Group(delimitedList(formOptionCheckbox, delim = LineEnd()))

        formOptionDropDown = types + formText
        formOptionDropDownList = Group(delimitedList(formOptionDropDown, delim = LineEnd()))

        formOptionPriority = types + formText
        formOptionPriorityList = Group(delimitedList(formOptionPriority, delim = LineEnd()))

        formOptionRng = types + formText
        formOptionRange = Group(delimitedList(formOptionRng, delim = Literal('|')))

        formOptions = delimitedList(formOptionShortText ^ 
                                    formOptionLongText ^ 
                                    formOptionParagraph ^ 
                                    formOptionRadiobuttons ^ 
                                    formOptionCheckboxes ^ 
                                    formOptionDropDownList ^ 
                                    formOptionRange ^ 
                                    formOptionPriorityList)

        formQuestion =  ZeroOrMore( Group ( formQuestionTitle + formOptions ) ) 
        formObject << (formTitle + formDescription + formQuestion)

        parse_result = formObject.parseString(self.s).asList()
        return parse_result

    def create_json(self):
        parse_result = self.parse_text()
        self.json_result["name"] = parse_result[0]
        self.json_result["description"] = parse_result[1]
        self.json_result["elements"] = list()

        for i in range(2, len(parse_result)):
            tmp = []
            q = OrderedDict()
            q2 = OrderedDict()
            for j, e in enumerate(parse_result[i]):
                if isinstance(e, list):
                    e = [el for n,el in enumerate(e) if el not in e[:n]]
                    tmp.append(e[0])
                    tmp.append(e[1:])
                else:
                    if e not in tmp:
                        tmp.append(e)

            if tmp[1] == "text" or tmp[1] == "textarea":
                q["text"] = tmp[0]
                q["type"] = tmp[1]
                q2["type"] = "question"
                q2["question"] = q
                self.json_result["elements"].append(q2)

            if tmp[1] == "paragraph":
                q["type"] = tmp[1]
                q["paragraph"] = OrderedDict({"html" : tmp[2]})
                self.json_result["elements"].append(q)

            if tmp[1] == "priority":
                q["text"] = tmp[0]
                q["type"] = tmp[1]
                q["priorityList"] = [OrderedDict({"value" : tmp[2][j]}) for j in range(len(tmp[2]))]
                q2["type"] = "question"
                q2["question"] = q
                self.json_result["elements"].append(q2)

            if (tmp[1] != "text") and (tmp[1] != "textarea") and (tmp[1] != "paragraph") and (tmp[1] != "priority"):
                q["text"] = tmp[0]
                q["type"] = tmp[1]
                q["offeredAnswers"] = [OrderedDict({"value" : tmp[2][j]}) for j in range(len(tmp[2]))]
                q2["type"] = "question"
                q2["question"] = q
                self.json_result["elements"].append(q2)
        return self.json_result

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-in', '--input', default='sample.txt')
    parser.add_argument('-out', '--output', default='output.txt')
    return parser
    
if __name__ == '__main__':
    parser = get_args()
    args = parser.parse_args(sys.argv[1:])

    pt = PlainText()
    pt.open_file(args.input)
    json_res = pt.create_json()
    res = json.dumps(json_res, indent=4)
    print(res, end='\n', file=open(args.output, 'w'))
        
